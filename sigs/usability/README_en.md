# MindSpore Usability Special Interest Group (SIG)

This is the working repo for the **Usability Special Interest Group (SIG)**. This repo contains all the artifacts, materials, meeting notes and proposals regarding **usability design, debugging and tuning ability, information experience, article content richness and quality** in MindSpore. Feedbacks and contributions are welcomed.

- **Usability Design**: including API usability, richness of operators and models, installation supports on multiple OS (Windows/Linux/Mac)

- **Debugging and Tuning Usability**: including usability of error reporting, self-debugging, and performance/accuracy tuning

- **Information Experience**: including information experience of API documentation, user's guide, tutorial, codelabs and official website

- **Article Content Richness and Quality**: including content richness and quality of developer's cases, industry best practices and advanced technology articles

## SIG Leads

- Zhang Tong (Huawei)

## Logistics

- SIG leads will drive the meeting.  
- Meeting announcement will be posted on our gitee channel: https://gitee.com/mindspore/community/tree/master/sigs/usability
- Feedbacks and topic requests are welcomed by all.  

## Discussion

- Documents and artifacts: https://gitee.com/mindspore/community/tree/master/sigs/usability  

## Meeting notes

TBD
